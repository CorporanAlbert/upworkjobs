import numpy as np
import time

from sklearn.metrics import confusion_matrix

# Import MNIST example data
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets('MNIST_data',one_hot=True)

# Load classes

from mainpart2 import *

# define variables
pixels = 784
nodes_relu=256
no_class = 10
"""
So about the learning rate, set it up depending on which optimization method you are using.
If it's SGD, set it at 0.5. If it's adagrad use 0.01. If it's RMSProp or Adam then use
0.001 as the learning rate.
"""
learning_rate = 0.001
epochs = 15

"""
So this epoch decay is one way to decay the learning rate. If this is true, then it
will divide the learning rate by two, every N epochs, N being the value of the 
lrate_epoch_decay variable. In this case, it divides the learning rate every 5 epochs.
I usually set it to false though, since I set up another method of decay, in which the 
learning rate is divided by two, every time the test error doesn't improve.
"""
epoch_decay = False #Decay learning rate by epochs when this is True.
lrate_epoch_decay = 5 #Decay the learning rate every time the epoch number is divisible by this
batch_size = 100
train=True

class_names= [0,1,2,3,4,5,6,7,8,9]
np.random.seed(123)

X = mnist.train.images
y = mnist.train.labels

y_test = mnist.test.labels
X_test = mnist.test.images
y_true = np.argmax(y_test, axis=1)

output_W = np.random.randn(pixels, nodes_relu)/100
output_b = np.random.randn(nodes_relu)/100

output_w_2=np.random.randn(nodes_relu,nodes_relu)/100
output_b_2=np.random.randn(nodes_relu)/100

output_w_3=np.random.randn(nodes_relu,no_class)/100
output_b_3=np.random.randn(no_class)/100

linear_layer_1 = linear_layer (output_W, output_b)
relu_layer_1 = relu_layer()
linear_layer_2 = linear_layer (output_w_2, output_b_2)
relu_layer_2 = relu_layer()
linear_layer_3 = linear_layer (output_w_3, output_b_3)

cost_ent_derive = cost_entropy_derivative()

if train :
    start = time.time()
    train_errors = []
    test_errors = []
    for _ in range(epochs):
        perm = np.random.permutation(len(X))
        X = X[perm]
        y = y[perm]

        for i in range(len(X) // batch_size):

            x_batch = X[i * batch_size : (i + 1) * batch_size]
            y_batch = y[i * batch_size : (i + 1) * batch_size]
            
            """
            You can add noise to the linear layers by adding a ,1 on the parameters.
            You can also add dropout to the relu layers by adding a ,1 on its parameters.
            """
            y_predict = linear_layer_1.forward(x_batch,1)
            relu_y = relu_layer_1.forward(y_predict, 1)
            y_predict2 = linear_layer_2.forward(relu_y,1)
            relu_y2 = relu_layer_2.forward(y_predict2, 1)
            predict_3=linear_layer_3.forward(relu_y2,1)
            out_predict, loss_rate = cost_ent_derive.forward(predict_3, y_batch)

            # backward

            back_1 = cost_ent_derive.backward()
            back_2 = linear_layer_3.backward(back_1)
            back_3 = relu_layer_2.backward(back_2)
            back_4 = linear_layer_2.backward(back_3)
            back_5 = relu_layer_1.backward(back_4)
            back_6 = linear_layer_1.backward(back_5)

            """
            Here are the optimization methods. If you want to use one, you'll have to
            uncomment it. I also don't recommend using more than one optimization method.
            OR ELSE.
            """
            # SGD learn
            
            """
            linear_layer_1.gradient_params(back_5)
            linear_layer_1.train(learning_rate)
            linear_layer_2.gradient_params(back_3)
            linear_layer_2.train(learning_rate)
            linear_layer_3.gradient_params(back_1)
            linear_layer_3.train(learning_rate)
            """
            

            
            #Adagrad learn
            """
            linear_layer_1.gradient_params(back_5)
            linear_layer_1.adagrad(learning_rate)
            linear_layer_2.gradient_params(back_3)
            linear_layer_2.adagrad(learning_rate)
            linear_layer_3.gradient_params(back_1)
            linear_layer_3.adagrad(learning_rate)
        
            """
            #RMSprop learn
            """
            linear_layer_1.gradient_params(back_5)
            linear_layer_1.RMSprop(learning_rate)
            linear_layer_2.gradient_params(back_3)
            linear_layer_2.RMSprop(learning_rate)
            linear_layer_3.gradient_params(back_1)
            linear_layer_3.RMSprop(learning_rate)
            """
            
            #Adam learn
            
            linear_layer_1.gradient_params(back_5)
            linear_layer_1.adam(learning_rate)
            linear_layer_2.gradient_params(back_3)
            linear_layer_2.adam(learning_rate)
            linear_layer_3.gradient_params(back_1)
            linear_layer_3.adam(learning_rate)
            
       

            print('\r' + str(loss(out_predict, y_batch)), end='')

       
        y_train_predict = linear_layer_1.forward(X, 0)
        relu_y_train = relu_layer_1.forward(y_train_predict, 0)
        y_predict_train_2 = linear_layer_2.forward(relu_y_train,0)
        relu_y_train_2 = relu_layer_2.forward(y_predict_train_2, 0)
        predict_train_3 = linear_layer_3.forward(relu_y_train_2,0)

        y_test_predict = linear_layer_1.forward(X_test,0)
        relu_y_test = relu_layer_1.forward(y_test_predict, 0)
        y_predict_test_2 = linear_layer_2.forward(relu_y_test, 0)
        relu_y_test_2 = relu_layer_2.forward(y_predict_test_2, 0)
        predict_test_3 = linear_layer_3.forward(relu_y_test_2, 0)


        train_error = loss(predict_train_3, y)
        test_error = loss(predict_test_3, y_test)
        
        train_errors.append(train_error)
        test_errors.append(test_error)
        
        """
        Here is where the learning rate division happens. Normally, it's divided when
        the current test error is larger than the previous one.
        """
        if epoch_decay:
            if lrate_epoch_decay%_ == 0:
                learning_rate = learning_rate/2
        else:
            if test_errors[_-1] < test_errors[_]:
                learning_rate = learning_rate/2
        
        
        print('\rEnd of epoch, train error :  ', train_error, '  Test error : ', test_error)
    save(linear_layer_1,'2d_linear_layer_1.pkl')
    save(relu_layer_1 ,'2d_relu_layer_1.pkl')
    save(linear_layer_2,'2d_linear_layer_2.pkl')
    save(relu_layer_2,'2d_relu_layer_2.pkl')
    save(linear_layer_3,'2d_linear_layer_3.pkl')
else:

    linear_layer_1 = load('2d_linear_layer_1.pkl')
    relu_layer_1 = load('2d_relu_layer_1.pkl')
    linear_layer_2 = load('2d_linear_layer_2.pkl')
    relu_layer_2=load('2d_relu_layer_2.pkl')
    linear_layer_3=load('2d_linear_layer_3.pkl')

    y_predict = linear_layer_1.forward(X_test, 1)
    relu_y = relu_layer_1.forward(y_predict, 1)
    y_predict2 = linear_layer_2.forward(relu_y, 1)
    relu_y2 = relu_layer_2.forward(y_predict2, 1)
    predict_test_3 = linear_layer_3.forward(relu_y2, 1)


end = time.time()
y_true_pred=np.argmax(predict_test_3, axis=1)

# Compute confusion matrix

cnf_matrix = confusion_matrix(y_true, y_true_pred)
np.set_printoptions(precision=0)

# Plot normalized confusion matrix
plt.figure()
plot_confusion_matrix(cnf_matrix, classes=class_names, normalize=True,
                      title='Normalized confusion matrix')

plt.show()

#Plot the errors.
plot_errorline(train_errors,test_errors,epochs,
               title = "Training and test error over epochs")
plt.show()

#Finally, print the time it took to run all this in seconds.
print(end - start)


