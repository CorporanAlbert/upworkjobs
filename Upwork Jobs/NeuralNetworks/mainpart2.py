
import numpy as np
import pickle as pkl
import matplotlib.pyplot as plt

class linear_layer(object):

     def __init__(self, W, b):
            self.X = None
            self.y_predict = None
            self.W = W
            self.b = b
            self.dX = None
            self.dW = None
            self.db = None
            #This list stores  the change of the gradient of the weight and bias over time.
            self.caches = [np.zeros_like(W),np.zeros_like(b)]
            #Store the momentum(how much the gradient has changed since last time)
            #of  the weight and bias.
            self.momentums = [np.zeros_like(W),np.zeros_like(b)]
            #Store the rate of change of the gradients,like velocity in physics.
            self.velocities = [np.zeros_like(W),np.zeros_like(b)]
  


     def forward(self, X,train,rnoise=False):

            y_predict = np.add(np.dot(X, self.W), self.b)

            if train:
                if rnoise:
                    self.X = noise(X)
                else:
                    self.X = X
                self.y_predict = y_predict
                
            return y_predict



     def backward(self, dy):
            dX = np.dot(dy, self.W.T)
            return dX

     def gradient_params(self, dy):
            self.dW = np.dot(self.X.T, dy)
            self.db = np.sum(dy, axis=0)
            return self.dW, self.db

     #SGD
     def train(self, learning_rate):
            self.W -= (learning_rate / np.shape(self.X)[0]) * self.dW
            self.b -= (learning_rate / np.shape(self.X)[0]) * self.db
     

        
        
    #Adagrad optimization             
     def adagrad(self,learning_rate,epsilon = 1e-8):
         """
         Adagrad Optimization.
         Inputs: learning rate: the learning rate of the neural network,which changes the
         values of the parameters on each training batch.
         
         epsilon: A parameter to prevent division by 0. Best values go through
         10^-8 to  10^-4
         
         This optimization method stores and sums the changes of the gradients of the 
         parameters, and uses them to perform gradient descent, and change the 
         weight and bias. Basically, it performs LARGE updates for the paramaters 
         that aren't changing that much, and performs small updates for the parameters
         that are changing a lot, so they can reach a happy balance. It's good for
         sparse data.
         """
         #Update gradient history for Weights and Bias.
         self.caches[0] += self.dW ** 2
         self.caches[1] += self.db ** 2
                    
        #Update the Weight and the Bias
         self.W -= learning_rate * self.dW / (np.sqrt(self.caches[0]) + epsilon)
         self.b -= learning_rate * self.db / (np.sqrt(self.caches[1]) + epsilon)
    
    #RMSprop Optimization
     def RMSprop(self, learning_rate,decay_rate = 0.9,epsilon = 1e-8):
         """
         RMSprop Optimization.
         Inputs: learning rate: the learning rate of the neural network,which changes the
         values of the parameters on each training batch.
         
         decay_rate:  A slowing down paramater, so that the learning rate is reduced.
         
         This optimization method is very similar to Adagrad, but instead of having 
         an aggresive learning rate, it uses a decay rate to reduce the aggresiveness.
         This prevents the model from stopping learning too early.
         Basically, it tells the adagrad gradient to SLOW THE FUCK DOWN since it's going
         too fast. This accomplished by multiplying it by some decay, in this case 0.9.
         """
         #Update caches of weights and bias.
         self.caches[0] = decay_rate * self.caches[0] + (1 - decay_rate) * self.dW**2
         self.caches[1] = decay_rate * self.caches[1] + (1 - decay_rate) * self.db**2
         
         #Update the weights and bias.
         self.W -= learning_rate * self.dW / (np.sqrt(self.caches[0]) + epsilon)
         self.b -= learning_rate * self.db / (np.sqrt(self.caches[1]) + epsilon)
         
    #Adam optimization
     def adam(self,learning_rate,beta1 = 0.9,beta2=0.999,epsilon = 1e-8):
         """
         Adam optimization.
         
         Inputs: learning rate: the learning rate of the neural network,which changes the
         values of the parameters on each training batch.
         
         beta1: Smoothing parameter for momentum.
         beta2: Smoothing parameter for velocity.
         epsilon: Smoothing term to prevent division by 0.
         This optimization method is RMSprop on STEROIDS. It stores the velocity
         or  the change of the gradients, and the momentum or how much the gradient 
         has changed over time. Adam also uses not one but TWO smoothings, called beta1
         and beta2. Think of them like decay_rates but with nerdy names.Then, it takes
         all these parameters and changes the values of the weights and the bias, just
         like the previous optimization methods.
         """
         
         #Update momentum and velocity of the weights, and update the weights.
         self.momentums[0] = beta1*self.momentums[0] + (1-beta1) * self.dW 
         self.velocities[0] = beta2*self.velocities[0] + (1-beta2) * (self.dW**2)
         self.W -= learning_rate * self.momentums[0]/ (np.sqrt(self.velocities[0]) + epsilon)
         
         #Update momentum and velocity of the bias, and update the bias.
         self.momentums[1] = beta1*self.momentums[1] + (1-beta1) * self.db 
         self.velocities[1] = beta2*self.velocities[1] + (1-beta2) * (self.db**2)
         self.b -= learning_rate * self.momentums[1]/ (np.sqrt(self.velocities[1]) + epsilon)
         
class relu_layer(object):

     def __init__(self):

            self.X = None
            self.y= None
            self.m = None
            self.drop = False
            

     def forward(self, X, train,drop=False,p=0.5):
        """
         I added dropout here, which is the drop parameter and the p parameter.
         IF drop is true, then it will start randomly dropping the BASS, I mean neurons.
         The p is how many neurons will be kept, going from 0 to 1.
         
        """
        y = np.multiply(np.array(X > 0, dtype=np.float32), X )
        if train:
            self.X = X
            if drop:
                #If the dropout flag is true, Create the dropout matrix.
                self.drop = True
                self.m = dropout(X,p)
                y *= self.m
            self.y = y
            
        if self.drop and train==False:
            #If we are using dropout, and training is over, Multiply the y vector
            #by the value of p.
            y*= p
            self.y = y
        return y

     def backward(self, dy):
            dydx = np.array(self.X > 0, dtype=np.float32)
            if self.drop:
                #If using dropout, multiply this by the same dropout matrix.
                dx_ = np.multiply(dydx, dy) * self.m
            else:
                dx_ = np.multiply(dydx, dy)
            return dx_


def softmax(x): 
    
    e = np.exp(x)
    s = e.sum(axis=1)
    return e / np.repeat(s, x.shape[1]).reshape(x.shape)


class cost_entropy_derivative(object):

     def __init__(self):
            self.cost_entropy = None
            self.y_predict = None
            self.y = None

     def forward(self, y_predict, y):
            self.y_predict = softmax(y_predict)

            self.y = y

            cost_entropy = - np.sum(np.multiply(y, np.log(self.y_predict)))

            return self.y_predict, cost_entropy

     def backward(self):
            return self.y_predict - self.y


def save(obj, name):
    with open(name, 'wb') as f:
        pkl.dump(obj, f)

def load(name):
    with open(name, 'rb') as f:
        return pkl.load(f)


def dropout(X, p = 0.5):
    """
    Drops out a percentage of the activators, by turning their value to 0.
    Basically, create a matrix with the same shape as the original one, but with 
    random 0s scattered.
    """
    return np.random.binomial(1,p, size = X.shape)



def noise(X,noise_type="Gamma"):
    """
    Adds noise to the input matrix. If it's gaussian noise, had normal noise to the matrix.
    If it's gamma noise, add a gamma distributed noise. Both go from 0 to 1, Gaussian noise
    is centered around the average.
    """
    #Gaussian (normal) noise. 
    if noise_type == "Gaussian":
        return X + np.random.normal(0,1,X.shape)
    
    #Gamma noise.
    return X + np.random.gamma(1,0,X.shape) 

def loss(y_predict, y):

    accuracy = np.sum(np.equal(np.argmax(y_predict, axis=1), np.argmax(y, axis=1))) / float(np.shape(y)[0])

    return 1 - accuracy

def plot_confusion_matrix(cm, classes, normalize= True, title='Confusion matrix', cmap=plt.cm.Blues):

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

def plot_errorline(train_error,test_error,epochs,title):
    """
    Plot the errors of the training and test error in one neat little package.
    """
    x = list(range(1,epochs+1))
    x = np.linspace(1,epochs+1,epochs)
    plt.plot(x,train_error,color = "blue")
    plt.plot(x,test_error,color = "red")
    plt.xlabel("Epoch")
    plt.ylabel("Error")
    plt.title(title)

