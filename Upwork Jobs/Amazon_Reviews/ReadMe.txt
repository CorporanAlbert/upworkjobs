Packages used:

import numpy for Linear algebra.  
import itertools  For combinations
import matplotlib for  Plotting.
import re for Regular Expressions.
import TextBlob for sentiment analysis
import pandas for data frames.

Dictionary used:
en-sentiment.