# -*- coding: utf-8 -*-
"""
Created on Sun May  7 08:28:02 2017

@author: juan9
"""
import numpy as np #Linear algebra.  
import matplotlib.pyplot as plt # Plotting.
import re #Regular Expressions.
import pandas as pd #Dataframes
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics.pairwise import linear_kernel


###Stop word list creation.
#Read the file with the stopwords, and load the words.
stopwords = open("stop-word-list.csv","r").read() 
#Split that long comma separated list of words into a python list.
stopwords_list = stopwords.split(",") 
#Replace whitespaces in the list with nothing.
cleaned_words = [re.sub(" ","",word) for word in stopwords_list]
#Create a dinctionary with the stopwords for faster access. 
stopwords = dict.fromkeys(cleaned_words) 

def Clean_Text(text):
    
    #Remove all non letters.
    letters_only = re.sub("[^a-zA-Z]"," ", text )

    #Split into words, and turn them into lowercase.
    words = letters_only.lower().split()

    #Remove stopwords from review.
    removed_stops = [w for w in words if not w  in stopwords and len(w)!=1]
    
    #Join them back.
    return " ".join(removed_stops)


def Get_Word_Count(dataframe):
    #Get the word count for a particular dataframe.
    result = {}
    #Iterate through words of the review
    for words in dataframe.text:
        #Check if a word is already in the dictionary. If it is add 1 to the counter, else add it to the dict.
        for word in words.split(' '):
            if word not in result :
                result[word]= 1
            else:
                result[word]+=1
    #Convert dictionary to a dataframe, for better plotting.
    df = pd.DataFrame({"Word": list(result.keys()),"Count":list(result.values())})
    #Sort by count.
    return df.sort_values(by="Count",ascending=False)


def Set_Parameters():
    from matplotlib import rcParams
    rcParams['figure.figsize'] = (10, 6) # Size of plot
    rcParams['figure.dpi'] = 150 #Dots per inch of plot
    rcParams['lines.linewidth'] = 2 # Width of lines of the plot
    rcParams['axes.facecolor'] = 'white' #Color of the axes
    rcParams['font.size'] = 14 # Size of the text.
    rcParams['patch.edgecolor'] = 'white' #Patch edge color.
    rcParams['font.family'] = 'StixGeneral' #Font of the plot text.
    


def plot_words(dataframe,label):
    plot_df = dataframe.head(10)
    x = np.arange(0,10,1) #Get arbitrary x axis values.
    y = plot_df.Count #y axis will be the counts.
    plt.bar(x,y,color = "blue") # Create the bar plot with a blue color.
    plt.title(label)
    plt.xticks(x,plot_df.Word) # Replace arbitrary x values with the most common words.
    plt.show()# Show the plot!

def Calculate_tfidf(dataframe):
    #Convert all articles into a list of text.
    corpus = list(dataframe.text)
    #Create a TfIdf vectorizer with minimal document frequency 100%, and max document frequency 10%, for accuracy.
    vectorizer = TfidfVectorizer( min_df = 0.05, max_df = 0.1)
    #Calculate the sum of the weights for each label.
    vector_weights = vectorizer.fit_transform(corpus)
    #Get the real weight, by calculating the mean of the weights.
    weights= list(np.asarray(vector_weights.mean(axis=0)).ravel())
    #Convert it to a dataframe.
    df = pd.DataFrame({"Word":vectorizer.get_feature_names(),"Score":weights})
    #Sort them by score, and return it, and the matrix of scores.
    return df,vector_weights.toarray()

def plot_scores(dataframe,label):
    plot_df = dataframe.head(10)
    x = np.arange(0,10,1) #Get arbitrary x axis values.
    y = plot_df.Score #y axis will be the scores.
    plt.bar(x,y,color = "orange") # Create the bar plot with a blue color.
    plt.title(label)
    plt.xticks(x,plot_df.Word) # Replace arbitrary x values with the most common words.
    plt.show()# Show the plot!


def Find_Related_Docs(kernel_matrix,doc_id):
    #Returns top 3 related docs.
    #Calculate cosine distance similarities of the document by id.
    cosine_similarities = linear_kernel(kernel_matrix[[doc_id]], kernel_matrix).flatten()
    #Get the top 4 related documents, by getting the ones with the highest index.
    related_docs_indices = cosine_similarities.argsort()[:-5:-1]
    #Return the top 3 related documents, excluding the original.
    return related_docs_indices[1:4]


def Top_3_Docs(doc_type,original_text,Tech_vect,bus_vect,fashion_vect):
    #Return the top 3 related texts for each text in a specific category.
    #Empty variable.
    doc_vector = " "
    #Check the type of the text, and get the kernel matrix for the type of text.
    if doc_type == "Technology":
        doc_vector = Tech_vect
        
    elif doc_type == "Business":
        doc_vector = bus_vect
        
    else:
        doc_vector = fashion_vect
    
    #Store the results in a dict to convert it to a dataframe.
    result = { }
    #For each text, find it's 3 most related texts.
    for i in range(0,len(doc_vector)):
        related = Find_Related_Docs(doc_vector,i)
        texts = []
        #Use the original dataframe instead of the one with stopwords removed.
        for index in related:
            texts.append(original_text.text[index])
        result[i] = texts
        
        #Convert the result to a dataframe.
    return pd.DataFrame({"Text_ID":list(result.keys()),
                         "Related_Texts":list(result.values())}).set_index("Text_ID")
    
        

def Display_Text_AndRelated(text_id,Related_Dataframe,original):
    #Displays the text and related texts
    print("Original Text:")
    print(original.text[text_id])
    print(" ")
    print("Related Text:")
    print(" ")
    for text in Related_Dataframe.Related_Texts[text_id]:
        print(text)
        print(" ")
    

def Plot_Heatmap(kernel_matrix):
    #Create and plot the heatmap for the kernel matrix.
    plt.imshow(kernel_matrix,cmap='hot',interpolation='nearest')
    plt.show()



def encode_type(ttype):
    #Helper function to convert types into numbers for KNeighbors.
    return {"technology":1,
            "fashion&style":2,
            "business":3
    }[ttype]
    

def Plot_Accuracies(accuracy_list):
    #Plot the accuracy of the KNN model as  K increases.
    x = range(5,30)
    y = accuracy_list
    plt.plot(x,y)
    plt.title("Accuracy as K increases")
    plt.show()
    
def Optimize_KClassifier(text_count,labels):
    """
    Iterate through a lot of values of K, and find the value of K that gives the 
    best accuracy.
    """
    #Initialize Accuracy and best K in zero.
    best_accuracy = 0
    best_k = 0
    #List to store accuracies.
    accuracy_list = []
    #For Ks from 5 to 30.
    for k in range(5,30):
        #Train a K Neighbors classifier for this value of K.
        Kneigh_classifier = KNeighborsClassifier(n_neighbors=k)
        Kneigh_classifier.fit(text_count,labels)
        
        #Calculate the accuracy for this classifier and add it to the list.
        accuracy = Kneigh_classifier.score(text_count,labels)
        accuracy_list.append(accuracy)
        #Compare this accuracy to the best accuracy so far. If it's better, it's the new
        #best K and best accuuracy.
        if accuracy > best_accuracy:
            best_accuracy = accuracy
            best_k = k
    #Plot the accuracy while K changes.
    print("Best K is:" + str(best_k))
    Plot_Accuracies(accuracy_list)
    #Return the best value of k.
    return best_k

def Plot_Comparison(X,y,Classifier):
    train = pd.DataFrame(X)
    train["label"] = y

    train = train.as_matrix()
    lbls = train

    Z = Classifier.predict(X) 


    techs = lbls[Z.ravel() == 1]
    fashions = lbls[Z.ravel() == 2]
    business = lbls[Z.ravel() == 3]

    
    i = range(0,len(techs[:,6691]))
    j = range(0,len(fashions[:,6691]))
    k = range(0,len(business[:,6691]))
    plt.scatter(i,techs[:,6691],color ='red')
    plt.scatter(j,fashions[:,6691],color = 'blue')
    plt.scatter(k,business[:,6691],color = 'green')
    # Put the result into a color plot
    plt.show()

   
def Main_Event():
    #Read the csv file.
    data = pd.read_csv("data.csv",encoding='latin1')
    original = data.copy()
    #Task1
    #Clean the text and replace the old data file.
    cleaned_text = data.text.apply(lambda new_text: Clean_Text(new_text)) #Apply the function per row.
    data.text = cleaned_text
    
    #Task2
    Set_Parameters()
    #Get word count for each category.
    Tech_Words = Get_Word_Count(data.loc[data.type == "technology"])
    Fashion_Words = Get_Word_Count(data.loc[data.type == "fashion&style"])
    Business_Words = Get_Word_Count(data.loc[data.type == "business"])   
    #Plot it.
    plot_words(Tech_Words,"Top Technology words")
    plot_words(Fashion_Words,"Top Fashion words")
    plot_words(Business_Words,"Top Business words")
    
    #Task 3 
    Tech_tfidf,Tech_vect = Calculate_tfidf(data.loc[data.type == "technology"])
    Business_tfidf,bus_vect = Calculate_tfidf(data.loc[data.type == "business"])
    Fashion_tfidf,fashion_vect = Calculate_tfidf(data.loc[data.type == "fashion&style"])
    #Plot it.
    plot_scores(Tech_tfidf, "Technology, tf-idf")
    plot_scores(Business_tfidf, "Business, tf-idf")
    plot_scores(Fashion_tfidf, "Fashion, tf-idf")       
    
    #Task 4
    Tech_Related_Docs = Top_3_Docs("Technology",original,Tech_vect,bus_vect,fashion_vect)
    Business_Related_Docs = Top_3_Docs("Business",original,Tech_vect,bus_vect,fashion_vect)
    Fashion_Related_Docs = Top_3_Docs("Fashion",original,Tech_vect,bus_vect,fashion_vect)
    
    #Change the parameters of this function, depending on the doc you want to analyze.
    Display_Text_AndRelated(0,Tech_Related_Docs,original)
    
    #Task5
    Plot_Heatmap(fashion_vect)
    Plot_Heatmap(Tech_vect)
    Plot_Heatmap(bus_vect)
    
    #Task6
    #Create a new term-count as a vector.
    count_vectorizer = CountVectorizer(min_df=1)
    #Create the term-document frequency matrix for all the text.
    doc_term_matrix = count_vectorizer.fit_transform(list(data.text))
    #Transform the document term matrix into a numpy matrix.
    text_count = doc_term_matrix.toarray()
    #Encode the text types into numbers.
    labels = data.type.apply(lambda ttype: encode_type(ttype))
    
    #Get the best K using the optimizer.
    best_k = Optimize_KClassifier(text_count,labels)
    #Create a new classifier with the best K and train it.
    Best_KClassifier = KNeighborsClassifier(n_neighbors = best_k)
    Best_KClassifier.fit(text_count,labels)
    #Get and print the accuracy of this best classifier.
    accuracy = Best_KClassifier.score(text_count,labels)
    print(accuracy)
    #Plot the predictions of the classifier.
    Plot_Comparison(text_count,labels,Best_KClassifier)
    


def Plot_Comparison_Stem(X,y,Classifier):
    train = pd.DataFrame(X)
    train["label"] = y

    train = train.as_matrix()
    lbls = train

    Z = Classifier.predict(X) 


    techs = lbls[Z.ravel() == 1]
    fashions = lbls[Z.ravel() == 2]
    business = lbls[Z.ravel() == 3]

    
    i = range(0,len(techs[:,4849]))
    j = range(0,len(fashions[:,4849]))
    k = range(0,len(business[:,4849]))
    plt.scatter(i,techs[:,4849],color ='red')
    plt.scatter(j,fashions[:,4849],color = 'blue')
    plt.scatter(k,business[:,4849],color = 'green')
    # Put the result into a color plot
    plt.show()    
#Task 7

from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")
def Stem_Text(text):
    text = text.split(" ")
    word_list = [stemmer.stem(word) for word in text]
    return " ".join(word_list)
    

def Top_3_StemDocs(doc_type,original,Stem_Tech_vect,Stem_bus_vect,Stem_fashion_vect):
    doc_vector = " ",0
    if doc_type == "Technology":
        doc_vector = Stem_Tech_vect
        
    elif doc_type == "Business":
        doc_vector = Stem_bus_vect
        
    else:
        doc_vector = Stem_fashion_vect
    
    result = { }
    for i in range(0,len(doc_vector)):
        related = Find_Related_Docs(doc_vector,i)
        texts = []
        for index in related:
            texts.append(original.text[index])
        result[i] = texts
        
    return pd.DataFrame({"Text_ID":list(result.keys()),
                         "Related_Texts":list(result.values())}).set_index("Text_ID")

def Optimize_StemKClassifier(Stem_text_count,labels):
    best_accuracy = 0
    best_k = 0
    accuracy_list = []
    for k in range(5,30):
        Kneigh_classifier = KNeighborsClassifier(n_neighbors=k)
        Kneigh_classifier.fit(Stem_text_count,labels)
        accuracy = Kneigh_classifier.score(Stem_text_count,labels)
        accuracy_list.append(accuracy)
        if accuracy > best_accuracy:
            best_accuracy = accuracy
            best_k = k
    print("Best K is:" + str(best_k))
    Plot_Accuracies(accuracy_list)
    return best_k

def Stemmed_Analysis():
    """
    Repeat tasks 1 through 6, but by cleaning the words even further, by using 
    a Snowball Stemmer, which turns words like family, families, familiar, into a 
    base form like famil.
    
    All the step are basically the same as in the previous analysis.
    """
    
    data = pd.read_csv("data.csv",encoding='latin1')
    
    original = data.copy()
    cleaned_text = data.text.apply(lambda new_text: Clean_Text(new_text)) #Apply the function per row.
    data.text = cleaned_text
    
    data.text = data.text.apply(lambda text: Stem_Text(text))

    stemmed_dataset = data
    Stemmed_Tech_Words = Get_Word_Count(stemmed_dataset.loc[stemmed_dataset.type == "technology"])
    Stemmed_Fashion_Words = Get_Word_Count(stemmed_dataset.loc[stemmed_dataset.type == "fashion&style"])
    Stemmed_Business_Words = Get_Word_Count(stemmed_dataset.loc[stemmed_dataset.type == "business"])

    plot_words(Stemmed_Tech_Words,"Top Technology words")
    plot_words(Stemmed_Fashion_Words,"Top Fashion words")
    plot_words(Stemmed_Business_Words,"Top Business words")  
    
    Stem_Tech_tfidf,Stem_Tech_vect = Calculate_tfidf(stemmed_dataset.loc[stemmed_dataset.type == "technology"])
    Stem_Business_tfidf,Stem_bus_vect = Calculate_tfidf(stemmed_dataset.loc[stemmed_dataset.type == "business"])
    Stem_Fashion_tfidf,Stem_fashion_vect = Calculate_tfidf(stemmed_dataset.loc[stemmed_dataset.type == "fashion&style"])

    plot_scores(Stem_Tech_tfidf, "Technology, tf-idf")
    plot_scores(Stem_Business_tfidf, "Business, tf-idf")
    plot_scores(Stem_Fashion_tfidf, "Fashion, tf-idf")
   
    
    Tech_Related_StemDocs = Top_3_StemDocs("Technology",original,Stem_Tech_vect,Stem_bus_vect,Stem_fashion_vect)
    Business_Related_StemDocs = Top_3_StemDocs("Business",original,Stem_Tech_vect,Stem_bus_vect,Stem_fashion_vect)
    Fashion_Related_StemDocs = Top_3_StemDocs("Fashion",original,Stem_Tech_vect,Stem_bus_vect,Stem_fashion_vect)
    
    Display_Text_AndRelated(0,Tech_Related_StemDocs,original)
    
    Plot_Heatmap(Stem_fashion_vect)
    Plot_Heatmap(Stem_Tech_vect)
    Plot_Heatmap(Stem_bus_vect)
    
    count_vectorizer = CountVectorizer(min_df=1)
    doc_term_Stemmatrix = count_vectorizer.fit_transform(list(stemmed_dataset.text))
    Stem_text_count = doc_term_Stemmatrix.toarray()
    labels = data.type.apply(lambda ttype: encode_type(ttype))
    
    best_Stemk = Optimize_StemKClassifier(Stem_text_count,labels)
    Best_StemKClassifier = KNeighborsClassifier(n_neighbors = best_Stemk)   
    Best_StemKClassifier.fit(Stem_text_count,labels)
    accuracy = Best_StemKClassifier.score(Stem_text_count,labels)
    
    print(accuracy)
    Plot_Comparison_Stem(Stem_text_count,labels,Best_StemKClassifier)
    
    
Main_Event()
Stemmed_Analysis()