# -*- coding: utf-8 -*-
"""
Created on Thu May  4 06:37:18 2017

@author: juan9
"""

# -*- coding: utf-8 -*-
"""
Created on Wed May  3 10:40:16 2017

@author: jununez
"""

from urllib.parse import urlsplit
import urllib
from bs4 import BeautifulSoup
import re
user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
hdr={'User-Agent':user_agent,} 

bad_domains = {}

def Get_Links(query):   
    req = urllib.request.Request('https://duckduckgo.com/html/?' + query, headers=hdr) 
    response = urllib.request.urlopen (req).read()
    parser = BeautifulSoup(response,'lxml')
    return parser.find_all('a',attrs = {"href": True})

def Get_Important_Links(links):
    result = []
    for link in links:
        if "uddg" in link.get('href'):
            result.append(link.get('href'))
    return list(set(result[:20]))



def Bad_URL(domain):
    if domain in bad_domains:
        return True
    return False
  
def Clean_URL(url):
    uddg_index = url.find("uddg=")+5
    return urllib.parse.unquote(url[uddg_index:])          


def Find_Emails(urls):
    emails = []
    for url in urls:
        base_url = "{0.netloc}".format(urlsplit(url))
        domain = base_url.split(".")[1]
        if not Bad_URL(domain):
            print("Scraping: " + url)

            req= urllib.request.Request(url,headers = hdr)
        
            try:
                page = urllib.request.urlopen(req).read()
                parser = BeautifulSoup(page,'lxml')
                url_emails = [a["href"] for a in parser.select('a[href^=mailto:]')]
                if url_emails:
                    cleaned_emails = re.findall('[a-zA-Z0-9+_\-\.]+@[0-9a-zA-Z][.-0-9a-zA-Z]*.[a-zA-Z]+',str(url_emails))
                    emails.extend(cleaned_emails)
                
            except urllib.error.HTTPError as e:
                print ("Could not enter this website.")
                bad_domains[domain] = url
            except urllib.error.URLError as e:
                print("Could not enter this website.")
                bad_domains[domain] = url
            except IOError:
                print("Could not enter this website.")
                bad_domains[domain] = url
            except TimeoutError:
                print("Could not enter this website.")
                bad_domains[domain] = url
            except:
                print("Could not enter this website.")
                bad_domains[domain] = url
            
    return emails
        

def URL_Email_Crawler(query,type = 0):
    print("Finding information of " + query)
    query = urllib.parse.urlencode ( { 'q' : query } )
    links = Get_Links(query)
    valid = Get_Important_Links(links)
    clean_urls = []
    for v in valid:
        clean_urls.append(Clean_URL(v))
    emails = Find_Emails(clean_urls)
    
       
    
    if not emails:
        print("Found no emails")
    else:
        print("Found emails: " + str(emails))
      
    if(type == 1):
        return emails 
    else:
        if not clean_urls:
            print("Found no URLs")
        else:
            print("Found URLs: " + str(clean_urls))      
    return clean_urls,emails
