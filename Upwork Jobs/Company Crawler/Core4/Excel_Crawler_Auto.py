# -*- coding: utf-8 -*-
"""
Created on Wed May  3 10:46:03 2017

@author: jununez
"""
import pandas as pd
import os
import Company_Crawler as cc
import numpy as np
from urllib.parse import urlsplit
from difflib import SequenceMatcher



def Get_Best_URL(urls):
    #Based on a list of urls, return what it thinks is the best URL. 
    #The rule is to find the shortest url with that contains www. If there are none, the shortest url will be returned.
    min_length = np.inf
    best_url = None
    for url in urls:
        if "www." in url:
            if len(url) < min_length:
                min_length = len(url)
                best_url = url
            if "attorney" in url or "lawyer" in url:
                best_url = url

    
    if best_url is None:
        best_url = min(urls, key = len)
        
    print("Selecting " + best_url + " as url.")        
    return best_url

def Filter_Email(email):
    #Filter bad emails
    bad_words = ["businessweek","support","facebook","example"]
    for word in bad_words:
        if word in email:
            return False
    return True

def Prompt_URL(urls):
    best_url = " "
    if not urls:
        best_url = input("No URLs found, write anything as the url.")
    else:
        choice = -1
        for i in range(0,len(urls)):
            print(str(i+1) + "." + urls[i])
        while choice < 1 and choice > len(urls):
            try:
                choice = int(input("Choose the url by number"))
            except ValueError:
                print("Must type a number")
            if choice < 1 and choice > len(urls):
                print("Value must be in the list")
                    
        best_url = urls[choice - 1 ]
    return best_url

def Get_Best_Email(url,emails,full_name):
    #Based on a lit of emails, return what it thinks is the best email. If there are no emails return name.lastname@domainname
    #The rule is to find the email that most strongly resembles the user's name.  
    base_url = "{0.netloc}".format(urlsplit(url)) #Get only the domain name.
    names = full_name.lower().split(" ")    
    domain =  base_url.split('.')[1]
    #base_email = names[0] + "." + names[1] + "@" + domain
    base_email2 = names[0][0] + "." + names[1] + "@" + domain
    email = " "
    if not emails:
        return base_email2
    else:
        full_name = full_name.replace(" ","") #Replace whitespaces.
        max_score = -1
        for mail in emails:
            if Filter_Email(mail):
                mail_score = SequenceMatcher(None,full_name,mail.split("@")[0]).ratio()
                if mail_score > max_score:
                    email = mail
            if(domain in mail):
                print("Found domain name for emails, returning email with domain name")
                print(email)
                return email
    print("Selecting " + email + " as email.")                  
    return email

def Prompt_Email(url,emails,full_name):
    best_mail = " "
    base_url = "{0.netloc}".format(urlsplit(url)) #Get only the domain name.
    names = full_name.lower().split(" ")
    if not emails:
        email = names[0] + "." + names[1] + "@" + base_url
        choice = input("No emails found, use " + email + "?" )
        if "Y"  in choice:
            return email 
        else:
            return "Not Found"
    else:
        choice = -1
        for i in range(0,len(emails)):
            print(str(i+1) + "." + emails[i])
        while choice < 1 and choice > len(emails):
            try:
                choice = int(input("Choose the email by number"))
            except ValueError:
                print("Must type a number")
            if choice < 1 and choice > len(email):
                print("Value must be in the list")
                    
        best_mail = emails[choice - 1 ]
        
    return best_mail

def Prompt_AutoMode(AutoMode):
    choice = input("Change Auto Mode Y/N?")
    if "Y" in choice:
        AutoMode = not AutoMode
    return AutoMode
    
if __name__ == "__main__":
    path = os.path.dirname(os.path.abspath(__file__))
    file = open(path + "\\Last_Read.txt","r")
    last_read = int(file.read())
    file.close()
    excel_files =[]
    AutoMode = True
    
    for file in os.listdir(path):
        if file.endswith(".xlsx"):
            excel_files.append(os.path.join(path, file))
        
    df = pd.read_excel(excel_files[0])
    df= df.replace(np.nan, '', regex=True)
    
    last_read = last_read+1
    last_read = str(last_read)
    new_file = open(path + "\\Last_Read.txt","w")
    new_file.write(last_read)
    new_file.close()
    last_read = int(last_read)

    
    if last_read > 0:        
        df = df[last_read:]
    
    df["URL"] = ""
    df["EMAIL"] = ""
    
    emails= []
    urls = []
    for index, row in df.iterrows():
        person_emails=[]
        person_urls = []
        
        if  row.COMPANY:
            url,email_list = cc.URL_Email_Crawler(row.COMPANY)
            email_list.extend(cc.URL_Email_Crawler(row.COMPANY + " email",1))
            person_urls = url
            
            if AutoMode == False:
                best_url = Prompt_URL(person_urls)
            else:
                best_url = Get_Best_URL(person_urls)
            
            urls.append(best_url)
            email_list.extend(cc.URL_Email_Crawler(row.FIRST + " " +  (row.MIDDLE if row.MIDDLE else "" )+ " " +  row.LAST + " email",1))
           
            if AutoMode == False:
                best_email = Prompt_Email(best_url,email_list,row.FIRST + " " + row.LAST)
            
            else:
                best_email = Get_Best_Email(best_url,email_list,row.FIRST + " " + row.LAST)
            emails.append(best_email)        
       
        else:
            url,email_list = cc.URL_Email_Crawler(row.FIRST + " " +  (row.MIDDLE if  row.MIDDLE else "" )+ " " +  row.LAST)
            email_list.extend(cc.URL_Email_Crawler(row.FIRST + " " +  (row.MIDDLE if  row.MIDDLE else "" )+ " " +  row.LAST + " email",1))
           
            if AutoMode == False:
                best_url = Prompt_URL(url)
           
            else:
                best_url = Get_Best_URL(url)
            urls.append(best_url)
                
            if AutoMode == False:
                best_email = Prompt_Email(best_url,email_list,row.FIRST + " " + row.LAST)
            
            else:
                best_email = Get_Best_Email(best_url,email_list,row.FIRST + " " + row.LAST)
            emails.append(best_email)
        last_read += 1
        
        row.URL = best_url
        row.EMAIL = best_email
        new_df = pd.DataFrame(row)
        new_df = new_df.transpose()
        new_df.to_csv(path + "\\results.csv", mode = 'a', header = False)
        
        last_read = str(last_read)
        new_file = open(path + "\\Last_Read.txt","w")
        new_file.write(last_read)
        new_file.close()
        last_read = int(last_read)


    